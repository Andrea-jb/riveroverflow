package river_overflow;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.util.Iterator;

public class StationLoadData {

    public static void main(String[] args) throws Exception {

        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();

        Table table = dynamoDB.getTable("Station");

        JsonParser parser = new JsonFactory().createParser(new File("src/main/resources/stations.json"));

        JsonNode rootNode = new ObjectMapper().readTree(parser);
        Iterator<JsonNode> iter = rootNode.iterator();

        ObjectNode currentNode;

        while (iter.hasNext()) {
            currentNode = (ObjectNode) iter.next();

            int id = currentNode.path("id").asInt();

            try {

                table.putItem(new Item().withPrimaryKey("id", id)
                        .withJSON("descripcio",currentNode.path("descripcio").toString())
                        .withJSON("dataInstalacio", currentNode.path("dataInstalacio").toString())
                        .withJSON("ultimManteniment", currentNode.path("ultimManteniment").toString())
                        .withJSON("distancia", currentNode.path("distancia").toString())
                        .withJSON("ponderacio", currentNode.path("ponderacio").toString()));

                System.out.println("PutItem succeeded: " + id);

            }
            catch (Exception e) {
                System.err.println("Unable to add Station: " + id);
                System.err.println(e.getMessage());
                break;
            }
        }
        parser.close();

    }

}
