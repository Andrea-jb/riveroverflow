package river_overflow;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import java.io.File;
import java.time.Instant;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;



public class SensorDataLoadData {

    private Instant instant;

    public static void main(String[] args) throws Exception {

        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();

        Table table = dynamoDB.getTable("SensorData");

        //Cargo solo un fichero para pruebas
        JsonParser parser = new JsonFactory().createParser(new File("src/main/resources/SensorData/2019-04-25_00_03_10_sensorsData.json"));

        JsonNode rootNode = new ObjectMapper().readTree(parser);
        Iterator<JsonNode> iter = rootNode.iterator();

        ObjectNode currentNode;

        parser.getTokenLocation();

        while (iter.hasNext()) {
            currentNode = (ObjectNode) iter.next();

            int stationId = currentNode.path("stationId").asInt();

            try {

                table.putItem(new Item().withPrimaryKey("stationId", stationId)
                        .withJSON("id",currentNode.path("id").toString())
                        .withJSON("instant", currentNode.path("instant").toString())
                        .withJSON("waterLevel", currentNode.path("waterLevel").toString())
                        .withJSON("humidity", currentNode.path("humidity").toString())
                        .withJSON("temperature", currentNode.path("temperature").toString())
                        .withJSON("rain", currentNode.path("rain").toString()));

                System.out.println("PutItem succeeded: " + stationId);

            }
            catch (Exception e) {
                System.err.println("Unable to add Station: " + stationId);
                System.err.println(e.getMessage());
                break;
            }
        }
        parser.close();

    }

}
