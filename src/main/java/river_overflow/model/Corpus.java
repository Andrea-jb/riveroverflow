package river_overflow.model;

import java.util.List;

public class Corpus {

    private List<SensorData> sensorDataList;


    public Corpus() {
    }

    public List<SensorData> getSensorDataList() {
        return sensorDataList;
    }

    public void setSensorDataList(List<SensorData> sensorDataList) {
        this.sensorDataList = sensorDataList;
    }
}
