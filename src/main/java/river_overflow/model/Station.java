package river_overflow.model;

public class Station {

    private int id;
    private String description;
    private String date_maintenance;
    private String date_installation;
    private Double distance;
    private Double weighing; // ponderacion

    public Station() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate_maintenance() {
        return date_maintenance;
    }

    public void setDate_maintenance(String date_maintenance) {
        this.date_maintenance = date_maintenance;
    }

    public String getDate_installation() {
        return date_installation;
    }

    public void setDate_installation(String date_installation) {
        this.date_installation = date_installation;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getWeighing() {
        return weighing;
    }

    public void setWeighing(Double weighing) {
        this.weighing = weighing;
    }
}
