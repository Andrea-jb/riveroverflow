package river_overflow.model;

import java.util.List;

public class CorpusStation {

    private List<Station> stationList;

    public CorpusStation() {
    }

    public List<Station> getStationList() {
        return stationList;
    }

    public void setStationList(List<Station> stationList) {
        this.stationList = stationList;
    }
}
