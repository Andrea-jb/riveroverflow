package river_overflow.model;

/**
 * Tendencia
 * Entendido como los datos que se va a estudiar
 * Resultado de una serie de ecuaciones y consultas.
 */
public class Tendency {

    private int id_station;
    private int maxWL;
    private int minWL;
    private int avgWL;
    private int humidityPromedy;
    private double prediction;

    public Tendency() {
    }

    public int getId_station() {
        return id_station;
    }

    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    public int getMaxWL() {
        return maxWL;
    }

    public void setMaxWL(int maxWL) {
        this.maxWL = maxWL;
    }

    public int getMinWL() {
        return minWL;
    }

    public void setMinWL(int minWL) {
        this.minWL = minWL;
    }

    public int getAvgWL() {
        return avgWL;
    }

    public void setAvgWL(int avgWL) {
        this.avgWL = avgWL;
    }

    public int getHumidityPromedy() {
        return humidityPromedy;
    }

    public void setHumidityPromedy(int humidityDifference) {
        this.humidityPromedy = humidityDifference;
    }

    public double getPrediction() {
        return prediction;
    }

    public void setPrediction(double prediction) {
        this.prediction = prediction;
    }
}
