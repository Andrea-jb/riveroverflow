package river_overflow;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

public class DynamoDBFactory {
    public DynamoDB getInstance() {
        return this.getDynamoDB();
    }

    private DynamoDB getDynamoDB() {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials("access_key_id", "secret_key_id");
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "us-west-2"))
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();

        return new DynamoDB(client);
    }
}