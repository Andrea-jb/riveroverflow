package river_overflow;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;

import java.util.Arrays;

public class CreateTableTendencies {

    public static void main(String[] args) {
        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();
        String tableName = "Tendencies";

        try{
            System.out.println("Attempting to create table; please wait...");

            Table table = dynamoDB.createTable(tableName,
                    Arrays.asList(new KeySchemaElement("stationId", KeyType.HASH)),
                    Arrays.asList(new AttributeDefinition("stationId", ScalarAttributeType.N)),
                    new ProvisionedThroughput(1L, 1L));

            table.waitForActive();

            System.out.println("Success.  Table status: " + table.getDescription().getTableStatus());

        }catch (Exception e) {
            System.err.println("Unable to create table: ");
            System.err.println(e.getMessage());
        }
    }
}
