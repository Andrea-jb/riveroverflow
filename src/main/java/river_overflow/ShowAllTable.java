package river_overflow;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;

import java.util.Iterator;
import java.util.List;

public class ShowAllTable {

    public static void main(String[] args) {
        PrintTable();
    }

    public static void PrintTable() {
        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();


        Table table = dynamoDB.getTable("Tendencies");

        ScanSpec scanSpec = new ScanSpec().withProjectionExpression("stationId,instant,waterLevel");

        try {
            ItemCollection<ScanOutcome> items = table.scan(scanSpec);

            Iterator<Item> iter = items.iterator();
            while (iter.hasNext()) {
                Item item = iter.next();
                String stationId = item.getString("stationId");

                System.out.println("stationId: " + stationId);

                System.out.println("**********************************");
            }

        }
        catch (Exception e) {
            System.err.println("Unable to scan the table:");
            System.err.println(e.getMessage());
        }
    }
}
