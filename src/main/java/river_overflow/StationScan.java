package river_overflow;

import java.util.Iterator;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;

public class StationScan {
    public static void main(String[] args) {

        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();

        Table table = dynamoDB.getTable("Station");

        ScanSpec scanSpec = new ScanSpec().withProjectionExpression("id, descripcio, dataInstalacio, ultimManteniment," +
                " distancia, ponderacio" );

        try {
            ItemCollection<ScanOutcome> items = table.scan(scanSpec);

            Iterator<Item> iter = items.iterator();
            while (iter.hasNext()) {
                Item item = iter.next();
                System.out.println(item.toString());
            }
        }
        catch (Exception e) {
            System.err.println("Unable to scan the table:");
            System.err.println(e.getMessage());
        }

    }
}
