package river_overflow;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;

import java.util.Iterator;

public class SensorDataScan {
    public static void main(String[] args) {

        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();

        Table table = dynamoDB.getTable("SensorData");

        ScanSpec scanSpec = new ScanSpec().withProjectionExpression("id, stationId, instant, waterLevel," +
                " humidity, temperature, rain" );

        try {
            ItemCollection<ScanOutcome> items = table.scan(scanSpec);

            Iterator<Item> iter = items.iterator();
            while (iter.hasNext()) {
                Item item = iter.next();
                System.out.println(item.toString());
            }
        }
        catch (Exception e) {
            System.err.println("Unable to scan the table:");
            System.err.println(e.getMessage());
        }

    }

}
