package river_overflow;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import river_overflow.model.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class TendenciesLoadData {

    public static void main(String[]args)throws Exception{
        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();

        ObjectMapper mapper = new ObjectMapper();

        try {
            //Llamamos las tablas de las cuales se van a consultar y operar los datos
            Table tableStations = dynamoDB.getTable("Stations");
            Table tableSensorData = dynamoDB.getTable("SensorData");

            // Donde volcaremos los resultados:
            Table tableTendencies = dynamoDB.getTable("Tendencies");

            //Query
            QuerySpec specSensorData = new QuerySpec().withKeyConditionExpression("sensorId = :sId") // :sId nombre que se le da al atributo en la qeury para despues ser llamado
                    .withFilterExpression("sensorId = :sId").withValueMap(new ValueMap().withInt(":sId",1));
            ItemCollection<QueryOutcome> items = tableSensorData.query(specSensorData);
            Iterator<Item> sensorDataIterator = items.iterator();


            while(sensorDataIterator.hasNext()){
                System.out.println(sensorDataIterator.next().toJSON());
            }


            // Recorremos un fichero para las pruebas
            Corpus corpus = mapper.readValue(new File("src/main/resources/sensorData/2019-04-25_00_06_30_sensorsData.json"), Corpus.class);
            List<SensorData> sensorDataList = corpus.getSensorDataList();
           // Iterator<Item> sensorDataIterator = items.iterator();

            CorpusStation corpusStation = mapper.readValue(new File("src/main/resources/stations.json"), CorpusStation.class);
            List<Station> stationList = corpusStation.getStationList();
            Iterator<Station> stationIterator = stationList.iterator();

            //Lista de los atributos:
            List<Integer> waterLevelList = new ArrayList<Integer>();
            List<Double> humidityList = new ArrayList<Double>();
            List<Double> temperatureList = new ArrayList<Double>();
            List<Integer> rainList = new ArrayList<Integer>();
            int waterLevelPlus = 0;
            //-------------------------------------------------------

        /*La idea de este bucle es almacenar los valores del nivel del agua
            y la humedad, para almacenarlos en una Collection y poder
            hacer unso de las funciones max() y min();
          */
/*
            while (sensorDataIterator.hasNext()) {
                SensorData sensorData = sensorDataIterator.next();
                waterLevelList.add(sensorData.getWaterLevel());
                waterLevelPlus += sensorData.getWaterLevel();
                humidityList.add(sensorData.getHumidity());
                temperatureList.add(sensorData.getTemperature());
                rainList.add(sensorData.getRain());
            }*/


        //Bucle para realizar el punto 4 del ejercicio
            while(stationIterator.hasNext()){
                Station station = stationIterator.next();
            }


            Integer max = Collections.max(waterLevelList);
            Integer min = Collections.min(waterLevelList);

            // Aqui se almacenan los datos en el objeto Tendency
            Tendency tendency = new Tendency();
            tendency.setMaxWL(max);
            tendency.setMinWL(min);
            tendency.setAvgWL(waterLevelPlus/waterLevelList.size()); // sacamos la media
            //tendency.setHumidityPromedy();
            //tendency.setPrediction();



        } catch (IOException e) {e.printStackTrace();}
    }
}
