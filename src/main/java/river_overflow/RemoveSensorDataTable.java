package river_overflow;


import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;

public class RemoveSensorDataTable {

    public void removeWorldCloudTable() {
        DynamoDB dynamoDB = new DynamoDBFactory().getInstance();

        String tableName = "SensorData";


        try {
            Table table = dynamoDB.getTable("SensorData");
            table.delete();
        }
        catch (Exception e) {
            System.err.println("Unable to delete table: " + tableName);
            System.err.println(e.getMessage());
        }
    }

}
